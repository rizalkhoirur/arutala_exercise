using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class ManagerAR : MonoBehaviour
{
    public static ManagerAR Instance;

    [SerializeField] private GameObject buttonContainer;
    [SerializeField] private AudioSource audioSource;
    [Space(5)]
    [SerializeField] private Button autoManualButton;
    [SerializeField] private TextMeshProUGUI autoManualButtonText;
    [Space(5)]
    [SerializeField] private Button blinkButton;
    [SerializeField] private TextMeshProUGUI blinkButtonText;
    [Space(5)]
    [SerializeField] private Button fireButton;
    [SerializeField] private TextMeshProUGUI fireButtonText;
    [Space(5)]
    [SerializeField] private Button saveAndShareButton;
    [SerializeField] private TextMeshProUGUI saveAndShareButtonText;

    private Transform detectedObject;
    private ParticleSystem detectedFire;

    private bool isManual = false;
    public bool IsManual { get => isManual; }

    private void Update()
    {
        if (detectedObject == null)
            return;
    }

    private void Awake()
    {
        buttonContainer.SetActive(false);
        Instance = this;
        SetSaveAndShareButton();
    }
    public void ObjectDetected(ObjectController objDetected)
    {
        detectedObject = objDetected.transform;
        detectedObject.localScale = new Vector3(5, 5, 5);
        buttonContainer.SetActive(true);
        SetAutoButton();
        SetBlinkButton();

        audioSource.clip = objDetected.ClipObject;
        audioSource.Play();
    }
    public void ObjectUndetected()
    {
        detectedObject.GetComponent<MeshRenderer>().enabled = true;
        detectedObject = null;
        buttonContainer.SetActive(false);
        StopCoroutine("AutoRotateObjectIE");

        audioSource.Stop();
    }

    #region Auto Rotate
    public void SetAutoButton()
    {
        isManual = false;
        autoManualButtonText.text = "Manual Rotate";
        StartCoroutine("AutoRotateObjectIE");
        detectedObject.localScale = new Vector3(5, 5, 5);
        autoManualButton.onClick.RemoveAllListeners();
        autoManualButton.onClick.AddListener(() => SetManualButton());
    }
    public void SetManualButton()
    {
        isManual = true;
        autoManualButtonText.text = "Auto Rotate";
        StopCoroutine("AutoRotateObjectIE");
        detectedObject.rotation = new Quaternion(0, 0, 0, 0);
        autoManualButton.onClick.RemoveAllListeners();
        autoManualButton.onClick.AddListener(() => SetAutoButton());
    }
    private IEnumerator AutoRotateObjectIE()
    {
        while (true)
        {
            if (detectedObject != null)
            {
                detectedObject.Rotate(0, 50 * Time.deltaTime, 0);
                yield return null;
            }
        }
    }
    #endregion
    #region Blinking
    private void SetBlinkButton()
    {
        blinkButton.onClick.RemoveAllListeners();
        blinkButton.onClick.AddListener(() => StartBlink());

        blinkButtonText.text = "Start Blink";
    }
    public void StartBlink()
    {
        StartCoroutine("BlinkingIE");

        blinkButtonText.text = "Stop Blink";

        blinkButton.onClick.RemoveAllListeners();
        blinkButton.onClick.AddListener(() => StopBlink());
    }
    public void StopBlink()
    {
        StopCoroutine("BlinkingIE");

        detectedObject.GetComponent<MeshRenderer>().enabled = true;

        blinkButtonText.text = "Start Blink";

        blinkButton.onClick.RemoveAllListeners();
        blinkButton.onClick.AddListener(() => StartBlink());
    }

    private IEnumerator BlinkingIE()
    {
        MeshRenderer meshRendererObject = detectedObject.GetComponent<MeshRenderer>();
        while (true)
        {
            meshRendererObject.enabled = false;
            yield return new WaitForSeconds(.2f);
            meshRendererObject.enabled = true;
            yield return new WaitForSeconds(.2f);
        }
    }
    #endregion
    #region Fire
    public void FireDetected(ParticleSystem particle)
    {
        detectedFire = particle;

        fireButtonText.text = "Show Fire";

        fireButton.onClick.RemoveAllListeners();
        fireButton.onClick.AddListener(() => ShowFire());
    }
    public void FireUndetected()
    {
        detectedFire.gameObject.SetActive(false);
        detectedFire = null;
    }

    private void ShowFire()
    {
        if (detectedFire != null)
        {
            detectedFire.gameObject.SetActive(true);

            fireButtonText.text = "Hide Fire";

            fireButton.onClick.RemoveAllListeners();
            fireButton.onClick.AddListener(() => HideFire());
        }
    }
    private void HideFire()
    {
        if (detectedFire != null)
        {
            detectedFire.gameObject.SetActive(false);

            fireButtonText.text = "Show Fire";

            fireButton.onClick.RemoveAllListeners();
            fireButton.onClick.AddListener(() => ShowFire());
        }
    }

    #endregion
    #region Save And Share
    private void SetSaveAndShareButton()
    {
        saveAndShareButtonText.text = "Save And Share";

        saveAndShareButton.onClick.RemoveAllListeners();
        saveAndShareButton.onClick.AddListener(() => TakeScreenShot());
    }
    private void TakeScreenShot()
    {
        StartCoroutine(TakeScreenshotAndShare());
    }
    private IEnumerator TakeScreenshotAndShare()
    {

        yield return new WaitForEndOfFrame();
        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();

        DateTime time = DateTime.Now;
        string name = "Screenshot_"+time.ToString("HH:mm") + ".png";
        NativeGallery.Permission permission = NativeGallery.SaveImageToGallery(ss, "ArutalaGallery", name, (success, path) =>
        {
            new NativeShare().AddFile(path).Share();
        });

        Destroy(ss);
    }
    #endregion

    public void GoToMenu()
    {
        TransitionManager.Instance.FadeOut(() =>
        {
            SceneManager.LoadScene("Menu");
            TransitionManager.Instance.FadeIn(() => { });
        });
    }
}
