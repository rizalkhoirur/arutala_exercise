using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransitionManager : MonoBehaviour
{
	public static TransitionManager Instance;

	[SerializeField] private Image fadeImage;
	[SerializeField] private float speedFade;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(Instance);
		}
		else
		{
			Destroy(gameObject);
		}
	}
    public void FadeOut(System.Action nextAction = null)
    {
		StartCoroutine(FadeOutIE(() => nextAction()));
	}
	public void FadeIn(System.Action nextAction = null)
	{
		StartCoroutine(FadeInIE(() => nextAction()));
	}
	private IEnumerator FadeInIE(System.Action nextAction)
	{
		while (fadeImage.color.a > 0)
        {
			float fadeAmout = fadeImage.color.a - (speedFade * Time.deltaTime);
			fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, fadeAmout);
			yield return null;
		}
		fadeImage.gameObject.SetActive(false);
		nextAction?.Invoke();
	}
	private IEnumerator FadeOutIE(System.Action nextAction)
	{
		fadeImage.gameObject.SetActive(true);
		while (fadeImage.color.a < 1)
		{
			float fadeAmout = fadeImage.color.a + (speedFade * Time.deltaTime);
			fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, fadeAmout);
			yield return null;
		}
		nextAction?.Invoke();
	}
}
