using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    void Start()
    {
        TransitionManager.Instance.FadeIn(() => { });
    }

    public void GoToExercise1()
    {
        TransitionManager.Instance.FadeOut(() =>
        {
            SceneManager.LoadScene("AR");
            TransitionManager.Instance.FadeIn(() => { });
        });
    }
    public void GoToExercise2()
    {
        TransitionManager.Instance.FadeOut(() =>
        {
            SceneManager.LoadScene("Time"); 
            TransitionManager.Instance.FadeIn(() => { });
        });
    }
}
