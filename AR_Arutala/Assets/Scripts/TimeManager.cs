using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    [Header("24 to 12 Hours")]
    [SerializeField] private TMP_InputField inputField12Hour;
    [SerializeField] private TextMeshProUGUI textResult12Hour;
    [Header("12 to 24 Hours")]
    [SerializeField] private TMP_InputField inputField24Hour;
    [SerializeField] private TextMeshProUGUI textResult24Hour;

    public void Convert12HoursButton()
    {
        Convert12HoursFormat(inputField12Hour.text);
    }
    private void Convert12HoursFormat(string time)
    {
        if (time.Contains("PM") || time.Contains("AM"))
        {
            textResult12Hour.text = "Error";
            return;
        }

        int hours = 0, minute = 0, seconds = 0;
        string[] timeSplit = time.Split(char.Parse(":"));
        seconds = int.Parse(timeSplit[2]);
        minute = int.Parse(timeSplit[1]);
        hours = int.Parse(timeSplit[0]);

        if (hours >= 24 || minute >= 60 || seconds >= 60 || timeSplit.Length < 3)
        {
            textResult12Hour.text = "Error";
            return;
        }

        if (hours == 24)
        {
            hours = 0;
        }

        if (hours >= 12)
        {
            if (hours > 12)
                hours -= 12;
            textResult12Hour.text = hours.ToString("00") + ":" + minute.ToString("00") + ":" + seconds.ToString("00") + "PM";
        }
        else
        {
            textResult12Hour.text = hours.ToString("00") + ":" + minute.ToString("00") + ":" + seconds.ToString("00") + "AM";
        }
    }
    public void ConvertTo24HourButton()
    {
        ConvertTo24HoursFormat(inputField24Hour.text);
    }
    private void ConvertTo24HoursFormat(string time)
    {
        time = time.ToUpper();
        if (time.Contains("PM") || time.Contains("AM"))
        {
            string timeDate = time.Split(new string[] { "AM", "PM" }, StringSplitOptions.RemoveEmptyEntries)[0];

            string[] timeSplit = timeDate.Split(char.Parse(":"));
            int seconds = int.Parse(timeSplit[2]);
            int minute = int.Parse(timeSplit[1]);
            int hours = int.Parse(timeSplit[0]);

            if (hours > 12 || minute >= 60 || seconds >= 60 || timeSplit.Length < 3)
            {
                textResult24Hour.text = "Error";
                return;
            }

            if (time.Contains("PM"))
            {
                if (hours != 12)
                {
                    hours = hours + 12;
                }
            }
            else
            {
                if (hours == 12)
                {
                    hours = 0;
                }
            }
            textResult24Hour.text = hours.ToString("00") + ":" + minute.ToString("00") + ":" + seconds.ToString("00");
        }
    }
    public void GoToMenu()
    {
        TransitionManager.Instance.FadeOut(() =>
        {
            SceneManager.LoadScene("Menu");
            TransitionManager.Instance.FadeIn(() => { });
        });
    }
}
