using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController : MonoBehaviour
{
    public AudioClip ClipObject;

    void Update()
    {
        if (Input.touchCount == 2)
        {
            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);

            Vector2 touch1Prev = touch1.position - touch1.deltaPosition;
            Vector2 touch2Prev = touch2.position - touch2.deltaPosition;

            float prevMagnitude = (touch1Prev - touch2Prev).magnitude;
            float currentMagnitude = (touch1.position - touch2.position).magnitude;

            float dif = currentMagnitude - prevMagnitude;

            float scale = Mathf.Clamp(transform.localScale.x + (dif * 0.01f), 5, 50);
            transform.localScale = new Vector3(scale, scale, scale);
        }

        if (!ManagerAR.Instance.IsManual)
            return;

        if (Input.touchCount == 1)
        {
            Touch touch0 = Input.GetTouch(0);
            if (touch0.phase == TouchPhase.Moved)
            {
                transform.Rotate(0f, -touch0.deltaPosition.x, 0f);
            }
        }
    }
}
